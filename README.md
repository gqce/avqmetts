# AVQMETTS 
AVQMETTS code to perform adaptive variational quantum minimally entangled typical thermal states for finite temperature simulations of quantum lattice models.

### Description
* `avqmetts`: folder containing the python source codes. 
* `example/tfim/n6`: folder containing an example of 1D transverse-field Ising model of 6 sites. To run the calculation, type:

    python ../../../avqmetts/run.py

One can compare with log file in the same folder for output.

### Reference
* J. C. Getelina, N. Gomes, T. Iadecola, P. P. Orth, and Y.-X. Yao, Adaptive Variational Quantum Minimally Entangled Typical Thermal States for Finite Temperature Simulations, [SciPost Physics 15, 102 (2023).](https://scipost.org/SciPostPhys.15.3.102)
