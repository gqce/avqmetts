# author: Yongxin Yao (yxphysice@gmail.com)
import os
import numpy as np
import json
from qutip import qeye, sigmax, sigmay, sigmaz, tensor, Qobj
from timing import timeit


class model:
    '''qubit model defined in incar file.
    '''

    def __init__(self,
                 beta=None,
                 lx=1,
                 ly=1,
                 ):
        self._beta = beta
        self._lx, self._ly = lx, ly
        self._size = self._lx * self._ly
        self._pauli = [qeye(2), sigmax(), sigmay(), sigmaz()]
        self.load_incar()
        self.set_h()

    def get_h_expval(self, vec):
        '''
        get Hamiltonian expectation value.
        '''
        # convert the state vector vec to Q object if it is an array.
        if isinstance(vec, np.ndarray):
            vec = Qobj(vec)
        # return the real expectation value.
        return self._h.matrix_element(vec, vec).real

    def get_lowest_states(self):
        '''
        get the lowest three eigenvalues and eigenstates.
        '''
        eigvals = 3 if self._nqbit > 1 else 2
        w, v = self._h.eigenstates(eigvals=eigvals, sparse=self._nqbit > 4)
        return w, v

    def load_incar(self):
        self._incar = json.load(open("incar", "r"))
        self._nqbit = len(self._incar["ref_state"])

    def set_sops(self):
        '''
        set up site-wise sx, sy, sz operators.
        '''
        self._ops = {}
        self._ops["X"], self._ops["Y"], self._ops["Z"] = \
            get_sxyz_ops(self._nqbit, self._pauli)

    def exact_thermal_avg(self):
        eigvals = np.linalg.eigvalsh(self._h.full())
        return sum([w * np.exp(-self._beta*w) for w in eigvals]) / \
                     sum([np.exp(-self._beta*w) for w in eigvals])

    # @timeit
    def map_hamiltonian(self):
        '''
        Write down the original string of spin-S operators in terms of Pauli
        matrices
        '''

        pm_dict = {"I": self._pauli[0], "X": self._pauli[1],
                   "Y": self._pauli[2], "Z": self._pauli[3]}

        op_total = 0.
        for op in self._hs_list:
            coef, label = float(op.split('*')[0]), op.split('*')[1]
            op_total += float(coef)*tensor([pm_dict[x] for x in label])

        return op_total

    # @ timeit
    def set_h(self):
        self._hs_list = self._incar["h"]
        self.set_sops()
        self._h = self.map_hamiltonian()

    def label2op(self, label):
        str_list = [self._ops[x][i] for i, x in enumerate(label) if x != 'I']
        op = 1
        for x in str_list:
            op *= x
        return op


def get_sxyz_ops(nqbit, pauli):
    '''
    set up site-wise sx, sy, sz operators.
    '''

    sx_list, sy_list, sz_list = [], [], []
    op_list = [pauli[0] for i in range(nqbit)]
    for i in range(nqbit):
        op_list[i] = pauli[1]
        sx_list.append(tensor(op_list))
        op_list[i] = pauli[2]
        sy_list.append(tensor(op_list))
        op_list[i] = pauli[3]
        sz_list.append(tensor(op_list))
        # reset
        op_list[i] = pauli[0]
    return [sx_list, sy_list, sz_list]


if __name__ == '__main__':
    mdl = model()
