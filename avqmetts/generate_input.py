import os
import numpy as np
import pandas as pd
from itertools import count
from more_itertools import distinct_permutations
import json


class InputParam:
    def __init__(self, input_file=None):
        # Load parameters from a CSV file
        param = pd.read_csv(input_file)
        # Initialize instance variables with parameter values
        self._bc = param.at[0, 'boundary_cond']
        self._nqbits = param.at[0, 'nqbits']
        self._lx = param.at[0, 'lx']
        self._ly = param.at[0, 'ly']
        self._coupling = [0., 0., param.at[0, 'jz']]
        self._magf = [param.at[0, 'bx'], 0., param.at[0, 'bz']]
        self._ref_state = self._nqbits * '0'
        self._pool = self.generate_pool()
        # Create a JSON string containing parameters
        self._json = json.dumps({'h': self.spin_chain(),
                                 "pool": self._pool,
                                 'ref_state': self._ref_state,
                                 'lx': float(self._lx),
                                 'ly': float(self._ly)},
                                indent=4)
        # Write the JSON string to a file
        with open("incar", 'w') as f:
            f.write(self._json)

    def spin_chain(self):
        """
        Generate operator strings representing a spin chain based on 
        specified coupling and magnetic field operators.
        """ 
        # s_op: A list of operator symbols for X, Y, and Z.
        # id_str: A string representing the identity operator ('I') repeated for the number of qubits in the system.
        s_op = ['X', 'Y', 'Z']
        id_str = self._nqbits * 'I'

        # Initialize an empty list to store the generated operator strings for the spin chain
        op_str = []

        # Iterate over elements of s_op, representing coupling and magnetic field operators
        for i, J, B, op in zip(count(), self._coupling, self._magf, s_op):
            if J:
                # Handle cases when there is coupling (J) between spins
                if self._ly > 1:
                    # Handle systems with multiple rows (Ly > 1)
                    opl = [
                        f"{J}*{(i + j * self._ly) * 'I' + 2 * op + (self._nqbits - i - 2 - j * self._ly) * 'I'}" for j in range(self._lx)
                        for i in range(self._ly - 1)]

                    # Check for periodic boundary conditions and add corresponding operator strings
                    if self._bc == 'pbc' and self._ly > 2:
                        opl.extend(
                            [f"{J}*{(j * self._ly) * 'I' + op + (self._ly - 2) * 'I' + op + (self._nqbits - (j + 1) * self._ly) * 'I'}" for j in range(self._lx)])

                    # Append the operator strings to op_str
                    op_str.extend(opl)

                    opl = [
                        f"{J}*{(i + j * self._ly) * 'I' + op + (self._ly - 1) * 'I' + op + (self._nqbits - i - (j + 1) * self._ly - 1) * 'I'}"
                        for j in range(self._lx - 1) for i in range(self._ly)]

                    # Check for periodic boundary conditions and add corresponding operator strings
                    if self._bc == 'pbc' and self._lx > 2:
                        opl.extend(
                            [f"{J}*{j * 'I' + op + (self._nqbits - self._ly - 1) * 'I' + op + (self._ly - j - 1) * 'I'}" for j in range(self._ly)])

                    # Append the operator strings to op_str
                    op_str.extend(opl)
                else:
                    # Handle systems with only one row (Ly = 1)
                    opl = [
                        f"{J}*{id_str[:i] + 2 * op + id_str[i + 2:]}" for i in range(self._nqbits - 1)]

                    # Check for boundary conditions and add corresponding operator strings
                    if self._nqbits > 2:
                        if self._bc == 'twist-xy' and i < 2:
                            opl.append(f"-{J}*{op + id_str[1:-1] + op}")
                        elif self._bc == 'pbc':
                            opl.append(f"{J}*{op + id_str[1:-1] + op}")

                    # Append the operator strings to op_str
                    op_str.extend(opl)
            if B:
                # Handle magnetic field terms (B)
                opl = [
                    f"{B}*{id_str[:i] + op + id_str[i + 1:]}" for i in range(self._nqbits)]

                # Append the operator strings to op_str
                op_str.extend(opl)

        # Return the list of generated operator strings representing the spin chain
        return op_str


    def generate_pool(self):
        """
        Generate a pool of operator strings in terms of Pauli matrices 
        for a quantum system with number of qubits nqbits.
        """

        # Define a set of operator patterns to generate
        pool_set = ['Y', "YZ"]

        # Initialize an empty list to store generated operator strings
        comb = []

        # Iterate through the operator patterns in the pool_set
        for elem in pool_set:
            # Calculate the number of identity ('I') characters needed to reach nqbits characters
            neye = self._nqbits - len(elem)
            # Generate distinct permutations of the operator pattern with 'I' characters
            operator_strings = [''.join(p) for p in distinct_permutations(elem + neye * 'I')]

            # Add the generated operator strings to the comb list
            comb += operator_strings

        # Return the list of generated operator strings as the operator pool
        return comb


if __name__ == '__main__':
    InputParam(input_file='param.csv')
