import os
import numpy as np
from model import model
from generate_input import InputParam
from ansatz import ansatzSinglePool
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--noopspm", action="store_true",
                    help="use native qutip. not recommended. for debug only.")
parser.add_argument("-c", "--rcut", type=float, default=1.e-3,
                    help="McLachlan distance cut-off. dflt: 0.01")
parser.add_argument("-f", "--fcut", type=float, default=1.e-1,
                    help="invidual unitary cut-off ratio. dflt: 0.01")
parser.add_argument("-m", "--maxadd", type=int, default=5,
                    help="Max allowed unitaries to be added at one iteration. dflt: 5.")
parser.add_argument("-t", "--dt", type=float, default=0.02,
                    help="Time step size. dflt: 0.02")
args = parser.parse_args()

opspm = not args.noopspm

def execute(input_file, beta=0., nit=0, warm_up=0):
    param = InputParam(input_file=input_file)

    mdl = model(beta=beta, lx=param._lx, ly=param._ly)
    w, _ = mdl.get_lowest_states()
    print(f"ground-state energy: {w[0]}")
    print(f"exact thermal energy for beta={beta}: {mdl.exact_thermal_avg()}")

    ans = ansatzSinglePool(mdl,
                           opspm=opspm,
                           rcut=args.rcut,
                           fcut=args.fcut,
                           max_add=args.maxadd,
                           dt=args.dt,
                           nit=nit,
                           warm_up=warm_up,
                           )

    print(f"AVQMETTS thermal energy for {nit} samples: {ans.run()}")


if __name__ == "__main__":
    execute("param.csv", beta=2.0, nit=32, warm_up=10)

