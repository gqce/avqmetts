# author: Yongxin Yao (yxphysice@gmail.com)
import numpy as np
from qutip import basis, tensor, qeye
from qutip.qip.operations import snot
from scipy.linalg import lstsq
from scipy.sparse.linalg import expm_multiply
# import warnings
# from .timing import timeit


class ansatz:
    '''
    define the main procedures of one step of avqite calculation.
    set up: 1) default reference state of the ansatz.
            2) operator pool
    '''

    def __init__(self,
                 model,           # the qubit model
                 opspm=True,      # operator in scipy sparse matrix
                 rcut=1.e-2,      # McLachlan distance cut-off
                 fcut=1.e-2,      # cut-off ratio for invidual contribution
                 max_add=5,       # maximal number of new pauli rotation gates to be added
                 dt=0.02,         # time step
                 nit=0,
                 warm_up=0,
                 ):
        self._model = model
        self._nq = model._nqbit
        self._lx = self._model._lx
        self._ly = self._model._ly
        self._nit = nit + warm_up
        self._warm_up = warm_up
        self._lmbd_tik = 1E-4
        self._opspm = opspm
        self._params = []   # variational parameters.
        self._ansatz = []   # list of unitaries in terms of operators and index labels
        self._state = None
        self._rcut = rcut
        self._rcut0 = rcut * fcut
        self._max_add = max_add
        self._dt = dt
        self.setup_pool()   # generate operator pool
        if isinstance(self._pool[0][1], str):
            self._max_pauli = max([len(x[1]) - x[1].count('I')
                                   for x in self._pool[:]])
        else:
            self._max_pauli = max([len(x) - x.count('I')
                                   for y in self._pool[:] for x in y[1]])
        self._ngates = self._max_pauli * [0]
        self.set_hadamard()
        self.set_ref_state()    # set reference state
        self._beta = self._model._beta

    @property
    def state(self):
        return self._state

    @property
    def ngates(self):
        return self._ngates[:]

    def update_state(self):
        self._state = self.get_state()

    # @timeit
    def setup_pool(self):
        '''
        setup pool from incar.
        '''
        raise NotImplementedError

    def set_hadamard(self):
        self._Hops = [(tensor([snot() if i == j else qeye(2)
                               for i in range(self._nq)])).data.tocsr()
                      for j in range(self._nq)]

    def set_ref_state(self):
        '''set reference state from ref_state.inp file.
        '''
        ref = self._model._incar["ref_state"]
        vec = tensor([basis(2, int(s)) for s in ref])

        if self._opspm:
            arr_vec = np.asarray(vec)
            self._ref_state = np.asarray([arr_vec[i][0]
                                          for i, _ in enumerate(arr_vec)])
        else:
            self._ref_state = vec.copy()

    def update_ngates(self):
        '''
        update gate counts.
        '''
        labels = self._pool[self._ansatz[-1]][1]
        if isinstance(labels, str):
            labels = [labels]
        for label in labels:
            iorder = len(label) - label.count('I')
            self._ngates[iorder - 1] += 1

    def solve_equation(self, m, v):
        m = m + np.diag(len(m) * [self._lmbd_tik])
        try:
            x, res, rnk, s = lstsq(m, v)
            return x
        except np.linalg.LinAlgError:
            minv = np.linalg.inv(m)
            return minv.dot(v)

    # @timeit
    def add_ops(self):
        '''
        ansatz adaptively expanding procedure in avqds.
        '''
        icyc = 0
        eps = 1e-12
        # energy variance
        hvar = self._e2 - self._e**2
        mamat, vavec = self.get_mvhelp()
        ntheta = len(self._params)

        for _ in range(self._max_add):
            # number of parameters
            lp = len(self._params)
            # M matrix
            mmat = np.zeros((lp + 1, lp + 1))
            # block for the existing parameters stay the same.
            mmat[:lp, :lp] = self._mmat
            # V vector
            vvec = np.zeros((lp + 1))
            vvec[:lp] = self._vvec
            # McLachlan distance without energy variance
            val_max = self._distp
            ichoice = None
            mmat_ch = None

            best_scores = []
            for i, oplabel in enumerate(self._pool):
                op, label = oplabel
                # same op is the end one, skip
                if len(self._ansatz) > 0 and i == self._ansatz[-1]:
                    continue
                if ntheta > 0:
                    mmat[-1, :ntheta] = mamat[i, :ntheta]
                # get mmat addition
                vecp_add, acol = self.get_mmat_add(op)
                mmat[-1, ntheta:] = acol
                # lower symmetric part
                mmat[:, -1] = mmat[-1, :]
                vvec[-1] = vavec[i]

                dthdt = self.solve_equation(mmat, vvec)
                dist_p = (2 * vvec.dot(dthdt) -
                          dthdt.dot(mmat.dot(dthdt))).real

                if dist_p > self._distp + self._rcut0:
                    best_scores.append(dist_p - self._distp)
                    if dist_p > val_max or abs(dist_p - val_max) < eps:
                        # dist drop, larger time step, better condition
                        ichoice = i
                        val_max = dist_p
                        mmat_ch = mmat.copy()
                        vvec_ch = vvec.copy()
                        dthdt_ch = dthdt
                        vecp_add_ch = vecp_add

            dist = hvar - val_max
            if ichoice is None:
                # warnings.warn("dynamic ansatz cannot further improve.")
                break

            self._distp = val_max
            self._mmat = mmat_ch
            self._dthdt = dthdt_ch
            self._vvec = vvec_ch
            self._ansatz.append(ichoice)
            self.update_ngates()
            self._vecp_list.append(vecp_add_ch)
            self._params.append(0)

            if dist < self._rcut:
                break
            icyc += 1

    # @timeit
    def get_mvhelp(self):
        # setup helper array for m and v for ansatz expansion.
        # save possible additional columns for mmat
        ntheta = len(self._params)
        npool = len(self._pool)
        mamat = np.zeros((npool, ntheta))
        vavec = np.zeros(npool)
        # H |vec>
        if self._opspm:
            hvec = self._h.dot(self._state)
        else:
            hvec = self._h * self._state

        for i, oplabel in enumerate(self._pool):
            op, _ = oplabel
            if self._opspm:
                vecp_add = -0.5j * op.dot(self._state)
                mamat[i, :] = [np.vdot(v, vecp_add).real
                               for v in self._vecp_list]
                vavec[i] = -np.vdot(vecp_add, hvec).real
            else:
                vecp_add = -0.5j * op * self._state
                mamat[i, :] = [v.overlap(vecp_add).real
                               for v in self._vecp_list]
                vavec[i] = -vecp_add.overlap(hvec).real

        # clear the list
        self._vecp_list = []
        return mamat, vavec

    def get_state(self):
        raise NotImplementedError

    def change_basis(self, state):
        for op in self._Hops:
            state = op * state

        return state

    def collapse_state(self, odd, state):
        if odd:
            state = self.change_basis(state)

        pval = (state * state.conj()).real
        state = np.random.multinomial(1, pval).astype(complex)
        if odd:
            state = self.change_basis(state)

        return state

    # @timeit
    def run(self):
        # change the representation of H if necessary.
        if self._opspm:
            self._h = self._model._h.data.tocsr()
        else:
            self._h = self._model._h

        # op_mz = sum([0.5 * self._model._ops["Z"][i].data.tocsr()
        #              for i in range(self._nq)]) / self._nq

        mean_energy = 0.
        psi0 = self._ref_state
        for j in range(self._nit):
            self._tmax = 0.5 * self._beta
            self._params = []
            self._ansatz = []
            self._ngates = self._max_pauli * [0]
            self._state = None
            self._ref_state = psi0
            self.update_state()

            self._t = 0.0
            while True:
                # apply AVQITE step
                self.one_step()
                if abs(self._t - self._tmax) < 1e-12:
                    break

            # collapse into another product state by measurement
            psi0 = self.collapse_state((j + 1) % 2, self._state)

            # compute observables and append to list
            if j >= self._warm_up:
                mean_energy += self._state.dot(self._h.dot(self._state.conj())).real
                # mean_magnetization += self._state.dot(op_mz.dot(self._state.conj())).real

        return mean_energy / (self._nit - self._warm_up)

    # @timeit
    def one_step(self):
        self.set_par_states()
        amat = self.get_amat()
        cvec, e, e2 = self.get_cvec_phase()
        # McLachlan's principle, including global phase contribution
        m = amat.real
        # minus sign
        v = -cvec.real

        self._mmat = m
        self._vvec = v
        self._e, self._e2 = e, e2

        if len(v) > 0:
            dthdt = self.solve_equation(m, v)
        else:
            dthdt = np.asarray([])
        # McLachlan distance
        dist_p = (2 * v.dot(dthdt) - dthdt.dot(m.dot(dthdt))).real
        dist_h2 = 2 * (e2 - e**2)     # added factor 2
        dist = dist_h2 - dist_p
        self._distp = dist_p
        if len(dthdt) > 0:
            pthmax = np.max(np.abs(dthdt))
        else:
            pthmax = 0
        self._pthmax = pthmax
        self._dthdt = dthdt
        if dist > self._rcut:
            self.add_ops()
        dt = self._dt
        if len(self._dthdt) > 0:
            if self._t + dt > self._tmax:
                dt = self._tmax - self._t
        self._t += dt
        self._params = [p + pp * dt for p,
                        pp in zip(self._params, self._dthdt)]
        self.update_state()

    def get_dist(self):
        return self._e2 - self._e**2 - self._distp

    # @timeit
    def set_par_states(self):
        ''' d |vec> / d theta.
        '''
        raise NotImplementedError

    def get_amat(self):
        lp = len(self._params)
        amat = np.zeros((lp, lp), dtype=complex)
        for i in range(lp):
            for j in range(i, lp):
                if self._opspm:
                    zes = np.vdot(self._vecp_list[i], self._vecp_list[j])
                else:
                    zes = self._vecp_list[i].overlap(self._vecp_list[j])
                amat[i, j] = zes
                if i != j:
                    # hermitian component
                    amat[j, i] = np.conj(zes)
        return amat

    def get_cvec_phase(self):
        lp = len(self._params)
        cvec = np.zeros(lp, dtype=complex)
        # h |vec>
        if self._opspm:
            hvec = self._h.dot(self._state)
            for i in range(lp):
                cvec[i] = np.vdot(self._vecp_list[i], hvec)
            # energy
            e = np.vdot(self._state, hvec).real
            e2 = np.vdot(hvec, hvec).real
        else:
            hvec = self._h * self._state
            for i in range(lp):
                cvec[i] = self._vecp_list[i].overlap(hvec)
            # energy
            e = (self._state.overlap(hvec)).real
            e2 = (hvec.overlap(hvec)).real

        return cvec, e, e2


class ansatzSinglePool(ansatz):
    '''
    adaptvqite with single operator pool.
    '''
    # @timeit

    def setup_pool(self):
        '''
        setup pool from incar.
        '''
        labels = self._model._incar["pool"]
        self._pool = [[self._model.label2op(s), s] for s in labels]

        if self._opspm:
            # convert to more efficient data structure if needed.
            self._pool = [[op[0].data.tocsr(), op[1]] for op in self._pool]

    def get_mmat_add(self, op):
        # mmat addition
        if self._opspm:
            vecp_add = -0.5j * op.dot(self._state)
            acol = [np.vdot(v, vecp_add).real
                    for v in self._vecp_list]
        else:
            vecp_add = -0.5j * op * self._state
            acol = [v.overlap(vecp_add).real for v in self._vecp_list]
        acol.append(0.25)

        return vecp_add, acol

    def get_state(self):
        return get_ansatz_state(self._params,
                                self._ansatz,
                                self._pool,
                                self._ref_state,
                                )

    # @timeit
    def set_par_states(self):
        ''' d |vec> / d theta.
        '''
        lp = len(self._params)
        vecp_list = []
        vec_i = self._ref_state
        if self._opspm:
            for i in range(lp):
                th, op = self._params[i], self._pool[self._ansatz[i]][0]
                # factor of 0.5 difference from the main text.
                prod = op.dot(vec_i)
                a, b = np.cos(0.5 * th), np.sin(0.5 * th)
                vec = -0.5j * a * prod - 0.5 * b * vec_i
                vec_i = a * vec_i - 1j * b * prod
                for j, th in enumerate(self._params[i + 1:]):
                    op = self._pool[self._ansatz[j + i + 1]][0]
                    vec = np.cos(0.5 * th) * vec - \
                        1j * np.sin(0.5 * th) * op.dot(vec)
                vecp_list.append(vec)
        else:
            for i in range(lp):
                th, op = self._params[i], self._pool[self._ansatz[i]][0]
                opth = -0.5j * th * op
                vec_i = opth.expm() * vec_i
                vec = -0.5j * op * vec_i
                for j, th in enumerate(self._params[i + 1:]):
                    opth = -0.5j * th * self._pool[self._ansatz[j + i + 1]][0]
                    vec = opth.expm() * vec
                vecp_list.append(vec)
        self._vecp_list = vecp_list


def get_ansatz_state(params, ansatz, pool, ref_state):
    state = ref_state
    opspm = isinstance(state, np.ndarray)
    for i, theta in enumerate(params):
        op = pool[ansatz[i]][0]
        if opspm:
            # use self-inverse of Pauli string
            state = np.cos(0.5 * theta) * state - \
                1j * np.sin(0.5 * theta) * op.dot(state)
        else:
            opth = -0.5j * theta * op
            state = opth.expm() * state
    return state

